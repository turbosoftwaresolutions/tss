function autoHeight() {
    $('#content').css('min-height', 0);
    $('#content').css('min-height', (
        $(document).height() 
            - $('#header').height() 
            - $('footer').height()
    ));
}

$(document).ready(function() {
    autoHeight();
});

$(window).resize(autoHeight);