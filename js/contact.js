$(function() {
    $('#contact').submit(function(e) {
        e.preventDefault();
        var loader = $('.tenor-gif-embed');
        $(this).hide();
        loader.show();
        $.ajax({
            url: '/mail',
            method: 'post',
            data: $(this).serialize(),
            success: function() {
                $('#mail-success').show();
            },
            error: function(err) {
                $('#mail-error').show();
                console.log(err);
            },
            complete: function() {
                loader.hide();
            }
        });
    });
});