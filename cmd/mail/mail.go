package main

import (
	"fmt"
	"log"
	"net/http"
	"net/smtp"
	"os"
)

func main() {
	username := os.Getenv("TSS_SMTP_USERNAME")
	password := os.Getenv("TSS_SMTP_PASSWORD")
	auth := smtp.PlainAuth("", username, password, "smtp.gmail.com")
	const to = "gavin@turbosoftwaresolutions.com"

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if err := r.ParseForm(); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		email := r.Form.Get("email")
		name := r.Form.Get("name")
		phone := r.Form.Get("phone")
		message := r.Form.Get("message")
		body := fmt.Sprintf("Someone filled out the contact form!\n\nName: %s\nEmail: %s\nPhone: %s\n\nMessage: %s",
			name, email, phone, message)
		fullMessage := fmt.Sprintf("From: %s\nTo: %s\nSubject: TSS Contact Form\n%s", username, to, body)

		err := smtp.SendMail("smtp.gmail.com:587", auth, username, []string{to}, []byte(fullMessage))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(200)
	})
	log.Fatal(http.ListenAndServe(":2000", http.DefaultServeMux))
}
